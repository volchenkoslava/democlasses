﻿using FishFactory.Domain.Abstract;
using FishFactory.Helpers;
using FishFactory.Infrastructure.Calculate;
using FishFactory.Infrastructure.Logging;
using FishFactory.Models.CalculateModels;
using FishFactory.Models.DomainModel;
using FishFactory.Models.PoolsModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FishFactory.Domain.Concrete
{
    public class CalculateRepository : IDisposable, ICalculateRepository
    {
        private FishFactoryEntities db;
        private ILogging logger;
        public CalculateRepository( FishFactoryEntities context,
                                    ILogging logger)
        {
            this.db = context;
            this.logger = logger;
        }

        /// <summary>
        /// Новый расчет
        /// </summary>
        /// <param name="poolId"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        public ResultHitchPoolRecord CalculateHitchPool(int poolId, DateTime dateStart, DateTime dateEnd, int settings)
        {
            try
            {
                #region Объявление локальных переменных
                PoolInfoModel pool = null;
                List<Stage_type> listStage = null;
                bool check = false;
                double amountFood = 0.0;
                double amountFoodStart = 0.0;
                double amountFoodEnd = 0.0;

                double inc = 0.0;
                double incStart = 0.0;
                double incEnd = 0.0;
                double incPeriod = 0.0;
                double densityStart = 0.0;
                double densityEnd = 0.0;

                double incrementCalcPeriod = 0;
                double incrementCalcNotSoldKillingPeriod = 0;
                double kkoef = 0;
                int diff = 0;
                #endregion

                #region Запрос SQl
                List<HitchPoolRecord> records = null;
                using (var db = new FishFactoryEntities())
                {
                    var selectPool = db.Pools.FirstOrDefault(x => x.id == poolId);
                    if (selectPool == null) return null;

                    pool = new PoolInfoModel(selectPool);

                    var stageList = db.Stage_type.ToList();
                    if (stageList != null) listStage = stageList;

                    records = db.Daily_record.Where(x => x.pool == pool.PoolId && x.Daily_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x))
                        .Union(db.Reception_record.Where(x => x.pool == pool.PoolId && x.Reception_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x)))
                        .Union(db.ControlHarvest_record.Where(x => x.pool == pool.PoolId && x.ControlHarvest_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x)))
                        .Union(db.Moving_record.Where(x => x.pool == pool.PoolId && x.Moving_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x, DirectionType.Increase)))
                        .Union(db.Moving_record.Where(x => x.pool_target == pool.PoolId && x.Moving_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x, DirectionType.Reduction)))
                        .Union(db.Sold_record.Where(x => x.pool == pool.PoolId && x.pool != null && x.Sold_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x)))
                        .Union(db.Killing_record.Where(x => x.pool == pool.PoolId && x.Killing_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x)))
                        .Union(db.Inventory_record.Where(x => x.pool == pool.PoolId && x.Inventory_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x)))
                        .Union(db.ChangeStage_record.Include(x => x.ChangeStage_card).Where(x => x.pool == pool.PoolId && x.ChangeStage_card.date <= dateEnd).AsEnumerable()
                            .Select(x => new HitchPoolRecord(x))).ToList();
                }
                #endregion

                #region Объявление модели
                HitchPool actualHitch = new HitchPool();
                HitchPool startResultHitch = new HitchPool();
                HitchPool endResultHitch = new HitchPool();
                HitchPool periodResultHitch = new HitchPool();
                HitchPool wasteHitch = new HitchPool();
                HitchPool wasteHitchStart = new HitchPool();
                HitchPool wasteHitchEnd = new HitchPool();
                HitchPool wasteHitchPeriod = new HitchPool();
                HitchPool movingHitch = new HitchPool();
                HitchPool movingHitchStart = new HitchPool();
                HitchPool movingHitchEnd = new HitchPool();
                HitchPool movingHitchPeriod = new HitchPool();
                HitchPool soldHitch = new HitchPool();
                HitchPool soldHitchStart = new HitchPool();
                HitchPool soldHitchEnd = new HitchPool();
                HitchPool soldHitchPeriod = new HitchPool();
                HitchPool killingHitch = new HitchPool();
                HitchPool killingHitchStart = new HitchPool();
                HitchPool killingHitchEnd = new HitchPool();
                HitchPool killingHitchPeriod = new HitchPool();
                HitchPool receptionHitch = new HitchPool();
                HitchPool receptionHitchStart = new HitchPool();
                HitchPool receptionHitchEnd = new HitchPool();
                HitchPool receptionHitchPeriod = new HitchPool();
                HitchPool inventoryHitch = new HitchPool();
                HitchPool inventoryHitchStart = new HitchPool();
                HitchPool inventoryHitchEnd = new HitchPool();
                HitchPool inventoryHitchPeriod = new HitchPool();
                HitchPool differenceHitch = new HitchPool();
                HitchPool differenceHitchStart = new HitchPool();
                HitchPool differenceHitchEnd = new HitchPool();
                HitchPool differenceHitchPeriod = new HitchPool();
                HitchPool doneWorkHitch = new HitchPool();
                HitchPool doneWorkHitchStart = new HitchPool();
                HitchPool doneWorkHitchEnd = new HitchPool();
                HitchPool doneWorkHitchPeriod = new HitchPool();

                StagePool stagePool = new StagePool { stageId = listStage.FirstOrDefault().id, stageName = listStage.FirstOrDefault().name, selectStageDate = (DateTime)pool.OpenDate };
                StagePool stagePoolStart = new StagePool();
                StagePool stagePoolEnd = new StagePool();
                List<StagePool> listStagePool = new List<StagePool>();
                TemperatureInfo temperature = new TemperatureInfo();
                TemperatureInfo temperatureStart = new TemperatureInfo();
                TemperatureInfo temperatureEnd = new TemperatureInfo();
                List<TemperatureInfo> listTemperature = new List<TemperatureInfo>();


                ResultHitchPoolRecord resultHitchPoolRecord = new ResultHitchPoolRecord(pool)
                {
                    // Актуальные навески
                    startResultHitch = startResultHitch,
                    endResultHitch = endResultHitch,
                    periodResultHitch = periodResultHitch,
                    // Отход
                    wasteHitchStart = wasteHitchStart,
                    wasteHitchEnd = wasteHitchEnd,
                    wasteHitchPeriod = wasteHitchPeriod,
                    // Перемещение
                    movingHitchStart = movingHitchStart,
                    movingHitchEnd = movingHitch,
                    movingHitchPeriod = movingHitchPeriod,
                    // Продажа
                    soldHitchStart = soldHitchStart,
                    soldHitchEnd = soldHitchEnd,
                    soldHitchPeriod = soldHitchPeriod,
                    // Забой 
                    killingHitchStart = killingHitchStart,
                    killingHitchEnd = killingHitchEnd,
                    killingHitchPeriod = killingHitchPeriod,
                    // Поступление
                    receptionHitchStart = receptionHitchStart,
                    receptionHitchEnd = receptionHitchEnd,
                    receptionHitchPeriod = receptionHitchPeriod,
                    // Инвентаризированно
                    inventoryHitchStart = inventoryHitchStart,
                    inventoryHitchEnd = inventoryHitchEnd,
                    inventoryHitchPeriod = inventoryHitchPeriod,
                    // Расхождение
                    differenceHitchStart = differenceHitchStart,
                    differenceHitchEnd = differenceHitchEnd,
                    differenceHitchPeriod = differenceHitchPeriod,
                    // Прирост расчетный
                    incrementCalc = incrementCalcPeriod,
                    incrementCalcNotSoldKilling = incrementCalcNotSoldKillingPeriod,
                    // Прирост по суточнйо карте
                    increment = inc,
                    incrementStart = incStart,
                    incrementEnd = incEnd,
                    incrementPeriod = incPeriod,
                    // Стадии
                    stagePool = stagePool,
                    stagePoolStart = stagePoolStart,
                    stagePoolEnd = stagePoolEnd,
                    listStagePool = listStagePool,
                    // Кормовой коэф
                    kk = kkoef,
                    //Температура
                    temperature = temperature,
                    temperatureInfoStart = temperatureStart,
                    temperatureInfoEnd = temperatureEnd,
                    listTemperature = listTemperature,
                    // Работа
                    doneWork = doneWorkHitch,
                    doneWorkStart = doneWorkHitchStart,
                    doneWorkEnd = doneWorkHitchStart,
                    doneWorkPeriod = doneWorkHitchPeriod
                };
                #endregion

                if (records != null && records.Count() > 0)
                {
                    var groupRecords = records.GroupBy(x => new { x.cardType, x.cardId, x.cardDate }).OrderBy(x => x.Key.cardDate);
                    foreach (var item in groupRecords)
                    {
                        #region Поиск начальной навески
                        if (!check && DateTime.Compare(item.Key.cardDate, dateStart) > 0)
                        {
                            // Начальная навеска
                            startResultHitch.count = actualHitch.count;
                            startResultHitch.avgHitch = actualHitch.avgHitch;
                            startResultHitch.biomassa = actualHitch.biomassa;
                            densityStart = pool.Square != 0 ? Math.Round(actualHitch.biomassa / pool.Square, 1) : 0;
                            startResultHitch.density = densityStart;
                            // Статус закрытия бассейна 
                            resultHitchPoolRecord.closePoolStart = (pool.CloseDate == null ? false : (DateTime.Compare((DateTime)pool.CloseDate, dateStart) < 1 ? true : false));
                            // Перемещение на начальный период
                            movingHitchStart.count = movingHitch.count;
                            movingHitchStart.biomassa = movingHitch.biomassa;
                            // Отход для начального периода
                            wasteHitchStart.count = wasteHitch.count;
                            wasteHitchStart.biomassa = wasteHitch.biomassa;
                            // Продажа для начального периода
                            soldHitchStart.count = soldHitch.count;
                            soldHitchStart.biomassa = soldHitch.biomassa;
                            // Забой для начального периода
                            killingHitchStart.count = killingHitch.count;
                            killingHitchStart.biomassa = killingHitch.biomassa;
                            // Поступление для начального периода
                            receptionHitchStart.count = receptionHitch.count;
                            receptionHitchStart.biomassa = receptionHitch.biomassa;
                            // Инвентаризация для начального периода
                            inventoryHitchStart.count = inventoryHitch.count;
                            inventoryHitchStart.biomassa = inventoryHitch.biomassa;
                            // Расхождение для начального периода
                            differenceHitchStart.count = differenceHitch.count;
                            differenceHitchStart.biomassa = differenceHitch.biomassa;
                            // Прирост для начального периода
                            incStart = inc;
                            // Расход корма для начального периода
                            amountFoodStart = amountFood;
                            resultHitchPoolRecord.amountFoodStart = amountFood;
                            // Стадия
                            stagePoolStart = stagePool;
                            resultHitchPoolRecord.stagePoolStart = stagePool;
                            // Температруа
                            temperatureStart = temperature;
                            resultHitchPoolRecord.temperatureInfoStart = temperature;
                            // Работа
                            doneWorkHitchStart.count = doneWorkHitch.count;
                            doneWorkHitchStart.biomassa = doneWorkHitch.biomassa;

                            check = true;
                        }
                        #endregion

                        using (var db = new FishFactoryEntities())
                        {
                            switch (item.Key.cardType)
                            {
                                case CardTypeNumber.DailyCard:
                                    #region Суточная карта
                                    var dailyRecord = db.Daily_record.Where(x => x.id == item.FirstOrDefault().recordId).FirstOrDefault();
                                    if (dailyRecord != null)
                                    {
                                        // Отход
                                        wasteHitch.count -= dailyRecord.Hitches.count;
                                        wasteHitch.biomassa -= dailyRecord.Hitches.biomass;
                                        // Текущая навеска
                                        actualHitch.count -= dailyRecord.Hitches.count;
                                        if (actualHitch.count != 0)
                                            actualHitch.biomassa -= dailyRecord.Hitches.biomass;
                                        else
                                            actualHitch.ZeroValues();
                                        // Корм
                                        amountFood += dailyRecord.Food_consumptions1.food_mass;
                                        // температура
                                        temperature.temperature = dailyRecord.water_temp != null ? (double)dailyRecord.water_temp : 0.0;
                                        temperature.date = (DateTime)dailyRecord.date;
                                        listTemperature.Add(temperature);
                                    }
                                    break;
                                #endregion

                                case CardTypeNumber.ReceptionCard:
                                    #region Карта поступления
                                    var record = item.FirstOrDefault();
                                    receptionHitch.count += record.count;
                                    receptionHitch.biomassa += record.biomassa;
                                    // Текущая навеска
                                    actualHitch.count += record.count;
                                    if (actualHitch.count != 0)
                                    {
                                        actualHitch.biomassa += record.biomassa;
                                        actualHitch.avgHitch = Convert.ToInt32(actualHitch.biomassa / actualHitch.count * 1000);
                                    }
                                    else
                                    {
                                        actualHitch.ZeroValues();
                                    }
                                    break;
                                #endregion

                                case CardTypeNumber.ControlHarvestCard:
                                    #region Контрольный облов
                                    // Текущая навеска
                                    if (actualHitch.count != 0)
                                    {
                                        actualHitch.avgHitch = Convert.ToInt32(item.FirstOrDefault().avgHitch_1);
                                        actualHitch.biomassa = item.FirstOrDefault().avgHitch_1 * actualHitch.count / 1000;
                                    }
                                    else
                                    {
                                        actualHitch.ZeroValues();
                                    }
                                    break;
                                #endregion

                                case CardTypeNumber.MovingCard:
                                    #region Перемещение
                                    diff = 0;
                                    foreach (var movingRecord in item)
                                    {
                                        // Навеска перемещения
                                        if (movingRecord.direction == 0)
                                        {
                                            movingHitch.count -= movingRecord.count;
                                            movingHitch.biomassa -= movingRecord.biomassa;
                                        }
                                        else
                                        {
                                            movingHitch.count += movingRecord.count;
                                            movingHitch.biomassa += movingRecord.biomassa;
                                        }
                                        // Текущая навеска
                                        if (movingRecord.direction == 0)
                                        {
                                            #region Перемещение из садка
                                            //Проделанная работа
                                            doneWorkHitch.count += movingRecord.count;
                                            doneWorkHitch.biomassa += movingRecord.biomassa;
                                            //
                                            actualHitch.count -= movingRecord.count;
                                            if (actualHitch.count != 0)
                                                actualHitch.biomassa -= movingRecord.biomassa;
                                            else
                                                actualHitch.ZeroValues();
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Перемещение в садок
                                            // Случай если бассейн пустой и заполняют перемещением
                                            if (actualHitch.count == 0 && movingRecord.count != 0)
                                            {
                                                actualHitch.count += movingRecord.count;
                                                actualHitch.biomassa += movingRecord.biomassa;
                                                actualHitch.avgHitch = Convert.ToInt32(actualHitch.biomassa / actualHitch.count * 1000);
                                            }
                                            else
                                            {
                                                actualHitch.count += movingRecord.count;
                                                if (actualHitch.count != 0)
                                                    actualHitch.biomassa += movingRecord.biomassa;
                                                else
                                                    actualHitch.ZeroValues();
                                            }
                                            #endregion
                                        }
                                        // Поиск расхождения
                                        if (movingRecord.countDiff != diff)
                                            diff = movingRecord.countDiff;
                                    }
                                    differenceHitch.count -= diff;
                                    break;
                                #endregion

                                case CardTypeNumber.SoldCard:
                                    #region Продажа
                                    diff = 0;
                                    foreach (var soldRecord in item)
                                    {
                                        soldHitch.count -= soldRecord.count;
                                        soldHitch.biomassa -= soldRecord.biomassa;
                                        // Текущая навеска
                                        actualHitch.count -= soldRecord.count;
                                        if (actualHitch.count != 0)
                                            actualHitch.biomassa -= soldRecord.biomassa;
                                        else
                                            actualHitch.ZeroValues();
                                        // Поиск расхождения
                                        if (soldRecord.countDiff != diff)
                                            diff = soldRecord.countDiff;
                                    }
                                    differenceHitch.count -= diff;
                                    break;
                                #endregion

                                case CardTypeNumber.KillingCard:
                                    #region Забой
                                    diff = 0;
                                    foreach (var killingRecord in item)
                                    {
                                        killingHitch.count -= killingRecord.count;
                                        killingHitch.biomassa -= killingRecord.biomassa;
                                        // Текущая навеска
                                        actualHitch.count -= killingRecord.count;
                                        if (actualHitch.count != 0)
                                            actualHitch.biomassa -= killingRecord.biomassa;
                                        else
                                            actualHitch.ZeroValues();
                                        // Поиск расхождения
                                        if (killingRecord.countDiff != diff)
                                            diff = killingRecord.countDiff;
                                    }
                                    differenceHitch.count -= diff;
                                    break;
                                #endregion

                                case CardTypeNumber.InventoryCard:
                                    #region Инвентаризация
                                    inventoryHitch.count = inventoryHitch.count + (item.FirstOrDefault().count - actualHitch.count);
                                    inventoryHitch.biomassa = inventoryHitch.biomassa + ((item.FirstOrDefault().count * actualHitch.avgHitch / 1000.0) - actualHitch.biomassa);
                                    // Текущая навеска
                                    var tempCount = actualHitch.count;
                                    actualHitch.count = item.FirstOrDefault().count;

                                    if (actualHitch.count != 0)
                                    {
                                        actualHitch.biomassa = actualHitch.biomassa + ((item.FirstOrDefault().count - tempCount) * actualHitch.avgHitch / 1000.0);
                                        actualHitch.avgHitch = Convert.ToInt32(actualHitch.biomassa / actualHitch.count * 1000);
                                    }
                                    else
                                    {
                                        actualHitch.ZeroValues();
                                    }
                                    break;
                                #endregion

                                case CardTypeNumber.ChangeStageCard:
                                    #region Изменение стадии
                                    stagePool.stageId = item.FirstOrDefault().stageId;
                                    stagePool.stageName = item.FirstOrDefault().stageName;
                                    stagePool.selectStageDate = item.FirstOrDefault().cardDate;

                                    listStagePool.Add(stagePool);
                                    #endregion
                                    break;
                            }
                        }
                    }

                    #region Навеска на начальный период если не была найдена ранее
                    if (!check)
                    {
                        // Начальная навеска
                        startResultHitch.count = actualHitch.count;
                        startResultHitch.avgHitch = actualHitch.avgHitch;
                        startResultHitch.biomassa = actualHitch.biomassa;
                        densityStart = pool.Square != 0 ? Math.Round(actualHitch.biomassa / pool.Square, 1) : 0;
                        startResultHitch.density = densityStart;
                        // Статус закрытия бассейна 
                        resultHitchPoolRecord.closePoolStart = (pool.CloseDate == null ? false : (DateTime.Compare((DateTime)pool.CloseDate, dateStart) < 1 ? true : false));
                        // Перемещение на первый период
                        movingHitchStart.count = movingHitch.count;
                        movingHitchStart.biomassa = movingHitch.biomassa;
                        // Отход для первого периода
                        wasteHitchStart.count = wasteHitch.count;
                        wasteHitchStart.biomassa = wasteHitch.biomassa;
                        // Продажа для первого периода
                        soldHitchStart.count = soldHitch.count;
                        soldHitchStart.biomassa = soldHitch.biomassa;
                        // Забой для первого периода
                        killingHitchStart.count = killingHitch.count;
                        killingHitchStart.biomassa = killingHitch.biomassa;
                        // Поступление для первого периода
                        receptionHitchStart.count = receptionHitch.count;
                        receptionHitchStart.biomassa = receptionHitch.biomassa;
                        // Инвентаризация для первого периода
                        inventoryHitchStart.count = inventoryHitch.count;
                        inventoryHitchStart.biomassa = inventoryHitch.biomassa;
                        // Расхождение для первого периода
                        differenceHitchStart.count = differenceHitch.count;
                        differenceHitchStart.biomassa = differenceHitch.biomassa;
                        // Прирост для первого периода
                        resultHitchPoolRecord.incrementStart = inc;
                        // Расход корма для первго периода
                        amountFoodStart = amountFood;
                        resultHitchPoolRecord.amountFoodStart = amountFood;
                        // Изменение стадии
                        stagePoolStart = stagePool;
                        resultHitchPoolRecord.stagePoolStart = stagePool;
                        // температура
                        temperatureStart = temperature;
                        resultHitchPoolRecord.temperatureInfoStart = temperature;
                        // Работа
                        doneWorkHitchStart.count = doneWorkHitch.count;
                        doneWorkHitchStart.biomassa = doneWorkHitch.biomassa;
                    }
                    #endregion

                    #region Навеск на конечный период
                    endResultHitch.count = actualHitch.count;
                    endResultHitch.avgHitch = actualHitch.avgHitch;
                    endResultHitch.biomassa = actualHitch.biomassa;
                    densityEnd = pool.Square != 0 ? Math.Round(actualHitch.biomassa / pool.Square, 1) : 0;
                    endResultHitch.density = densityEnd;
                    resultHitchPoolRecord.closePoolEnd = false;

                    if (pool.CloseDate != null && DateTime.Compare((DateTime)pool.CloseDate, dateEnd) < 1)
                    {
                        endResultHitch.count = 0;
                        endResultHitch.avgHitch = 0;
                        endResultHitch.biomassa = 0.0;
                        densityEnd = 0.0;
                        endResultHitch.density = densityEnd;
                        resultHitchPoolRecord.closePoolEnd = true;
                    }
                    // отход на конечный период
                    wasteHitchEnd.count = wasteHitch.count;
                    wasteHitchEnd.biomassa = wasteHitch.biomassa;
                    // поступление на конечный период
                    receptionHitchEnd.count = receptionHitch.count;
                    receptionHitchEnd.biomassa = receptionHitch.biomassa;
                    // перемещение на конечный период
                    movingHitchEnd.count = movingHitch.count;
                    movingHitchEnd.biomassa = movingHitch.biomassa;
                    // Продажа на конечный период
                    soldHitchEnd.count = soldHitch.count;
                    soldHitchEnd.biomassa = soldHitch.biomassa;
                    // Забой на конечный период
                    killingHitchEnd.count = killingHitch.count;
                    killingHitchEnd.biomassa = killingHitch.biomassa;
                    // Инвентаризация на кончный период
                    inventoryHitchEnd.count = inventoryHitch.count;
                    inventoryHitchEnd.biomassa = inventoryHitch.biomassa;
                    // Расхождение на конечный период
                    differenceHitchEnd.count = differenceHitch.count;
                    differenceHitchEnd.biomassa = differenceHitch.biomassa;
                    // Прирост на конечный период
                    resultHitchPoolRecord.incrementEnd = inc;
                    // Расход корма на конечный период
                    amountFoodEnd = amountFood;
                    resultHitchPoolRecord.amountFoodEnd = amountFood;
                    // Стадии
                    stagePoolEnd = stagePool;
                    resultHitchPoolRecord.stagePoolEnd = stagePool;
                    // температру
                    temperatureEnd = temperature;
                    resultHitchPoolRecord.temperatureInfoEnd = temperature;
                    // Работа
                    doneWorkHitchEnd.count = doneWorkHitch.count;
                    doneWorkHitchEnd.biomassa = doneWorkHitch.biomassa;
                    #endregion

                    #region Навеска за период
                    periodResultHitch.count = endResultHitch.count - startResultHitch.count;
                    if (periodResultHitch.count != 0)
                    {
                        periodResultHitch.biomassa = endResultHitch.biomassa - startResultHitch.biomassa;
                        periodResultHitch.avgHitch = Convert.ToInt32(periodResultHitch.biomassa / periodResultHitch.count * 1000);
                    }
                    else
                    {
                        periodResultHitch.ZeroValues();
                    }
                    // Отход за период
                    wasteHitchPeriod.count = wasteHitchEnd.count - wasteHitchStart.count;
                    wasteHitchPeriod.biomassa = wasteHitchEnd.biomassa - wasteHitchStart.biomassa;
                    // Поступление за период
                    receptionHitchPeriod.count = receptionHitchEnd.count - receptionHitchStart.count;
                    receptionHitchPeriod.biomassa = receptionHitchEnd.biomassa - receptionHitchStart.biomassa;
                    // Перемещение за период
                    movingHitchPeriod.count = movingHitchEnd.count - movingHitchStart.count;
                    movingHitchPeriod.biomassa = movingHitchEnd.biomassa - movingHitchStart.biomassa;
                    // Продажа за период
                    soldHitchPeriod.count = soldHitchEnd.count - soldHitchStart.count;
                    soldHitchPeriod.biomassa = soldHitchEnd.biomassa - soldHitchStart.biomassa;
                    // Забой за период
                    killingHitchPeriod.count = killingHitchEnd.count - killingHitchStart.count;
                    killingHitchPeriod.biomassa = killingHitchEnd.biomassa - killingHitchStart.biomassa;
                    // Инвентаризация за период
                    inventoryHitchPeriod.count = inventoryHitchEnd.count - inventoryHitchStart.count;
                    inventoryHitchPeriod.biomassa = inventoryHitchEnd.biomassa - inventoryHitchStart.biomassa;
                    // расхождение за период
                    differenceHitchPeriod.count = differenceHitchEnd.count - differenceHitchStart.count;
                    differenceHitchPeriod.biomassa = differenceHitchEnd.biomassa - differenceHitchStart.biomassa;
                    // Корм за период
                    resultHitchPoolRecord.amountFoodPeriod = amountFoodEnd - amountFoodStart;
                    // Прирост за период
                    resultHitchPoolRecord.incrementPeriod = incEnd - incStart;
                    // Расчетный прирост за период
                    resultHitchPoolRecord.incrementCalc = periodResultHitch.biomassa + killingHitchPeriod.biomassa * (-1) + soldHitchPeriod.biomassa * (-1);
                    resultHitchPoolRecord.incrementCalcNotSoldKilling = periodResultHitch.biomassa - receptionHitchPeriod.biomassa;
                    // Стадии
                    resultHitchPoolRecord.listStagePool = listStagePool;
                    // Температура
                    resultHitchPoolRecord.listTemperature = listTemperature;
                    // Кормовой коэф
                    resultHitchPoolRecord.kk = (resultHitchPoolRecord.incrementCalc + wasteHitchPeriod.biomassa) != 0 ? resultHitchPoolRecord.amountFoodPeriod / (resultHitchPoolRecord.incrementCalc + wasteHitchPeriod.biomassa) : 0;
                    // Работа
                    doneWorkHitchPeriod.count = doneWorkHitchEnd.count - doneWorkHitchStart.count;
                    doneWorkHitchPeriod.biomassa = doneWorkHitchEnd.biomassa - doneWorkHitchStart.biomassa;
                    #endregion

                    return resultHitchPoolRecord;
                }
            }
            catch (Exception ex) {
                logger.WriteError(this.ToString(), ex.Message);
                ResultHitchPoolRecord resultHitchPoolRecord = new ResultHitchPoolRecord();
                resultHitchPoolRecord.SetError();
                return resultHitchPoolRecord;
            }
        }


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
