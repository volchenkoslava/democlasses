﻿using lab.Domain.Core.Models;
using lab.Domain.Interfaces;
using lab.Infrastructure.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using lab.Infrastructure.Types;

namespace lab.Domain.Data
{
    public class MeteringModeRepository : IMeteringModeRepository
    {
        private ApplicationDbContext db;
        private ILogging logger;

        public MeteringModeRepository(ILogging logger)
        {
            db = new ApplicationDbContext();
            this.logger = logger;
        }
        

        public void Add(MeteringMode item)
        {
            try
            {
                db.MeteringModes.Add(item);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        public void Delete(MeteringMode item)
        {
            try
            {
                var selectItem = db.MeteringModes.FirstOrDefault(x => x.Id == item.Id);
                if (selectItem != null)
                {
                    selectItem.Show = false;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        public void Edit(MeteringMode item)
        {
            try
            {
                var selectItem = db.MeteringModes.FirstOrDefault(x => x.Id == item.Id);
                if (selectItem != null)
                {
                    selectItem.Name = item.Name;
                    selectItem.Comment = item.Comment;
                    selectItem.Show = item.Show;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Получение MeteringMode по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MeteringMode GetMeteringMode(int id)
        {
            try
            {
                return db.MeteringModes.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Получение MeteringMode по типу
        /// </summary>
        /// <param name="meteringMode"></param>
        /// <returns></returns>
        public MeteringMode GetMeteringMode(MeteringModeEnum meteringMode)
        {
            try
            {
                return db.MeteringModes.FirstOrDefault(x => x.meteringModeEnum == (int)meteringMode);
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Получение активных MeteringMode
        /// </summary>
        /// <returns></returns>
        public List<MeteringMode> ListMeteringModeActive()
        {
            List<MeteringMode> listItems = new List<MeteringMode>();
            try
            {
                var selectItems = db.MeteringModes.Where(x => x.Show == true);
                if (selectItems != null && selectItems.Count() > 0)
                {
                    listItems = selectItems.OrderBy(x => x.Name).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return listItems;
        }

        /// <summary>
        /// Редактирование MeteringMode для DeviceType
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="meteringType"></param>
        /// <param name="listMeteringMode"></param>
        public void EditListMeteringModeDeviceType(DeviceType deviceType, MeteringType meteringType, List<MeteringMode> listMeteringMode)
        {
            try
            {
                var selectDeviceType = db.DeviceTypes.FirstOrDefault(x => x.Id == deviceType.Id);
                var selectMeteringtype = db.MeteringTypes.FirstOrDefault(x => x.Id == meteringType.Id);
                if (selectDeviceType != null && selectMeteringtype != null && listMeteringMode != null && listMeteringMode.Count() > 0)
                {
                    var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.Where(x => x.DeviceTypeId == deviceType.Id &&
                        x.MeteringTypeId == meteringType.Id);
                    List<MeteringMode> contentListMeteringMode = new List<MeteringMode>();
                    // Список содержащихся
                    foreach (var item in selectDTMTMM)
                    {
                        contentListMeteringMode.Add(item.MeteringMode);
                    }
                    // список на удаление
                    List<MeteringMode> deleteList = new List<MeteringMode>();
                    foreach (var itemContent in contentListMeteringMode)
                    {
                        bool check = false;
                        foreach (var itemAdd in listMeteringMode)
                        {
                            if (itemContent.Id == itemAdd.Id)
                                check = true;
                        }
                        if (check == false)
                            deleteList.Add(itemContent);
                    }
                    foreach (var item in deleteList)
                    {
                        var DTMTMM = db.DeviceType_MeteringType_MeteringModes.Include(x => x.TypeMeteringConnections).FirstOrDefault(x =>
                        x.DeviceTypeId == deviceType.Id &&
                        x.MeteringTypeId == meteringType.Id &&
                        x.MeteringModeId == item.Id
                        );
                        if (DTMTMM != null)
                        {
                            db.DeviceType_MeteringType_MeteringModes.Remove(DTMTMM);
                        }
                    }
                    // список на добавление
                    List<MeteringMode> addList = new List<MeteringMode>();
                    foreach (var addItem in listMeteringMode)
                    {
                        bool check = false;
                        foreach (var contentItem in contentListMeteringMode)
                        {
                            if (addItem.Id == contentItem.Id)
                                check = true;
                        }
                        if (!check)
                            addList.Add(addItem);
                    }
                    foreach (var item in addList)
                    {
                        var DTMTMM = new DeviceType_MeteringType_MeteringMode
                        {
                            DeviceTypeId = deviceType.Id,
                            MeteringTypeId = meteringType.Id,
                            MeteringModeId = item.Id
                        };
                        db.DeviceType_MeteringType_MeteringModes.Add(DTMTMM);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Список добавленных MeteringMode для DeviceType
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="meteringType"></param>
        /// <returns></returns>
        public List<MeteringMode> ListMeteringModeDeviceType(DeviceType deviceType, MeteringType meteringType)
        {
            List<MeteringMode> listMeteringMode = new List<MeteringMode>();
            try
            {
                var selectDeviceType = db.DeviceTypes.FirstOrDefault(x => x.Id == deviceType.Id);
                var selectMeteringType = db.MeteringTypes.FirstOrDefault(x => x.Id == meteringType.Id);
                if (selectDeviceType != null && selectMeteringType != null)
                {
                    var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.Include(x => x.MeteringMode).Where(x => x.DeviceTypeId == deviceType.Id &&
                    x.MeteringTypeId == meteringType.Id);
                    foreach (var item in selectDTMTMM)
                    {
                        listMeteringMode.Add(new MeteringMode
                        {
                            Id = item.MeteringMode.Id,
                            Name = item.MeteringMode.Name,
                            Comment = item.MeteringMode.Comment,
                            Show = item.MeteringMode.Show
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return listMeteringMode;
        }

        /// <summary>
        /// Список не добавленных MeteringMode для DeviceType
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="meteringType"></param>
        /// <returns></returns>
        public List<MeteringMode> ListMeteringModeNotDeviceType(DeviceType deviceType, MeteringType meteringType)
        {
            List<MeteringMode> listMeteringMode = new List<MeteringMode>();
            try
            {
                var selectDeviceType = db.DeviceTypes.FirstOrDefault(x => x.Id == deviceType.Id);
                var selectMeteringType = db.MeteringTypes.FirstOrDefault(x => x.Id == meteringType.Id);
                if (selectDeviceType != null && selectMeteringType != null)
                {
                    var contentMeteringMode = ListMeteringModeDeviceType(deviceType, meteringType);
                    var allMeteringMode = db.MeteringModes.ToList();
                    foreach (var itemAll in allMeteringMode)
                    {
                        bool check = false;
                        foreach (var itemContent in contentMeteringMode)
                        {
                            if (itemAll.Id == itemContent.Id)
                                check = true;
                        }
                        if (check == false)
                            listMeteringMode.Add(itemAll);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return listMeteringMode;
        }

        /// <summary>
        /// Добавление MeteringMolr для DeviceType
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="meteringType"></param>
        /// <param name="meteringMode"></param>
        /// <returns></returns>
        public bool AddMeteringModeInDeviceType(DeviceType deviceType, MeteringType meteringType, MeteringMode meteringMode)
        {
            bool result = false;
            try
            {
                var DTMTMM = new DeviceType_MeteringType_MeteringMode
                {
                    DeviceTypeId = deviceType.Id,
                    MeteringTypeId = meteringType.Id,
                    MeteringModeId = meteringMode.Id
                };
                db.DeviceType_MeteringType_MeteringModes.Add(DTMTMM);
                db.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Удаление MeteringMode у DeviceType
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="meteringType"></param>
        /// <param name="meteringMode"></param>
        /// <returns></returns>
        public bool DeleteMeteringModeInDeviceType(DeviceType deviceType, MeteringType meteringType, MeteringMode meteringMode)
        {
            bool result = false;
            try
            {
                var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.FirstOrDefault(x => x.DeviceTypeId == deviceType.Id &&
                                                                                           x.MeteringTypeId == meteringType.Id &&
                                                                                           x.MeteringModeId == meteringMode.Id);
                if (selectDTMTMM != null)
                {
                    db.DeviceType_MeteringType_MeteringModes.Remove(selectDTMTMM);
                    db.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return result;
        }

        public DeviceType_MeteringType_MeteringMode GetDTMTMM(DeviceType deviceType, MeteringType meteringType, MeteringMode meteringMode)
        {
            try
            {
                var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.Where(x =>
                x.DeviceTypeId == deviceType.Id &&
                x.MeteringTypeId == meteringType.Id &&
                x.MeteringModeId == meteringMode.Id).FirstOrDefault();
                return selectDTMTMM;
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);              
            }
            return null;
        }

        public DeviceType_MeteringType_MeteringMode GetDTMTMM(int deviceTypeId, int meteringTypeId, int meteringModeId)
        {
            try
            {
                var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.Where(x =>
                x.DeviceTypeId == deviceTypeId &&
                x.MeteringTypeId == meteringTypeId &&
                x.MeteringModeId == meteringModeId).FirstOrDefault();
                return selectDTMTMM;
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return null;
        }

        /// <summary>
        /// Получение MeteringMode для Device
        /// </summary>
        /// <param name="device"></param>
        /// <param name="meteringType"></param>
        /// <returns></returns>
        public List<MeteringMode> ListMeteringModeDevice(Device device, MeteringType meteringType)
        {
            List<MeteringMode> listMeteringMode = new List<MeteringMode>();
            try
            {
                var selectDevice = db.Devices.FirstOrDefault(x => x.Id == device.Id);
                if (selectDevice != null)
                {
                    var selectDTMTMM = db.DeviceType_MeteringType_MeteringModes.Include(x => x.MeteringMode).Where(x => x.DeviceTypeId == selectDevice.DeviceTypeId &&
                    x.MeteringTypeId == meteringType.Id);
                    foreach (var item in selectDTMTMM)
                    {
                        listMeteringMode.Add(item.MeteringMode);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
            return listMeteringMode;
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
