﻿using FishFactory.Domain.Abstract;
using FishFactory.Infrastructure;
using FishFactory.Infrastructure.Logging;
using FishFactory.Models.FishLogModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FishFactory.Controllers.FishLogControllers
{
    [Authorize]
    public class FishLogEveryMonthController : Controller
    {
        private ICalculateRepository сalculateRepository;
        private ISettingsRepository settingsRepository;
        private IPartyRepository partyRepository;
        private IReportRepository reportRepository;
        private ILogging logger;

        private const int counterMax = 10;

        public FishLogEveryMonthController(ICalculateRepository сalculateRepository,
                                            ISettingsRepository settingsRepository,
                                            IPartyRepository partyRepository,
                                            IReportRepository reportRepository,
                                            ILogging logger)
        {
            this.сalculateRepository = сalculateRepository;
            this.settingsRepository = settingsRepository;
            this.partyRepository = partyRepository;
            this.reportRepository = reportRepository;
            this.logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AllPartyInfo(string startDate, string endDate)
        {
            DateTime sDate = DateTime.Parse(startDate);
            DateTime eDate = DateTime.Parse(endDate).AddDays(1);

            var setting = settingsRepository.GetSettingsDefault("calc_hitch_method_in_log", 1);

            List<EveryMonthPartyModels> listEveryMonthParty = new List<EveryMonthPartyModels>();
            EveryMonthPartyModels resultEveryMonth = new EveryMonthPartyModels();
            EveryMonthModels everyMonth = new EveryMonthModels
            {
                StartDate = startDate,
                EndDate = endDate,
                ListEveryMonthPartyModel = listEveryMonthParty,
                ResultEveryMonthParty = resultEveryMonth
            };

            try
            {
                // Получение всех активных партий с бассейнами
                var listParty = partyRepository.GetListActiveParty(sDate, eDate);
                foreach (var party in listParty)
                {
                    var counter = 0;
                    List<EveryMonthPoolModels> listEveryMonthPool = new List<EveryMonthPoolModels>();
                    EveryMonthPartyModels everyMonthParty = new EveryMonthPartyModels
                    {
                        PartyId = party.Id,
                        PartyName = party.Name,
                        ListEveryMonthPools = listEveryMonthPool
                    };
                    // Добавление задач расчёта навесок по бассейнам в партии
                    List<Task<EveryMonthPoolModels>> tasks = new List<Task<EveryMonthPoolModels>>();
                    for (var i = 0; i < party.ListPool.Count; i++)
                    {
                        int index = i;
                        tasks.Add(new Task<EveryMonthPoolModels>(() =>
                        {
                            var calcPool = сalculateRepository.CalculateHitchPool(party.ListPool[index].PoolId, sDate, eDate, setting);
                            EveryMonthPoolModels model = new EveryMonthPoolModels(calcPool);
                            listEveryMonthPool.Add(model);
                            return model;
                        }));
                        counter++;
                        // Запуск одновременного расчета 10 задач
                        if (counter >= counterMax)
                        {
                            foreach (var task in tasks)
                            {
                                task.Start();
                            }
                            Task.WaitAll(tasks.ToArray());
                            foreach (var task in tasks)
                            {
                                ChangePartyModel(ref everyMonthParty, task.Result);
                            }
                            tasks.Clear();
                            counter = 0;
                        }
                    }
                    // разбор оставшихся задач
                    foreach (var t in tasks)
                    {
                        t.Start();
                    }
                    // Ожижание выполнения задач
                    Task.WaitAll(tasks.ToArray());
                    // Сохранение результатов расчета в модель
                    foreach (var task in tasks)
                    {
                        ChangePartyModel(ref everyMonthParty, task.Result);
                    }
                    tasks.Clear();
                    listEveryMonthParty.Add(everyMonthParty);
                }
                // Итого
                foreach (var item in listEveryMonthParty.OrderBy(x => x.PartyName))
                {
                    // Округление бассейна
                    RoundItemModel(item);

                    // Расчет итога
                    CalculateResult(resultEveryMonth, item);
                }
                // Округление итогов
                RoundResult(resultEveryMonth);

                everyMonth.ReportPath = reportRepository.CreateReport(ReportType.Excel, "FishLogEveryMonth", everyMonth);
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
                everyMonth.SetError(ex.Message);
                return Json(everyMonth);
            }
      
            return Json(everyMonth);
        }

        public FileResult Download(string reportName, string path)
        {
            if (path == "") return null;
            FileInfo fileInf = new FileInfo(path);
            if (!fileInf.Exists) return null;

            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = reportName + DateTime.Now.ToString("MMddHHmmss") + ".xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        /// <summary>
        /// Заполнение модели результатами расчета
        /// </summary>
        /// <param name="partyModel"></param>
        /// <param name="result"></param>
        private void ChangePartyModel(ref EveryMonthPartyModels partyModel, EveryMonthPoolModels result)
        {

            partyModel.CountStart += result.CountStartHitch;
            partyModel.BiomassaStart += result.BiomassaStartHitch;
            partyModel.ReceptionCount += result.ReceptCount;
            partyModel.ReceptionBiomassa += result.ReceptBiomassa;
            partyModel.SoldCount += result.SoldCount;
            partyModel.SoldBiomassa += result.SoldBiomassa;
            partyModel.KillingCount += result.KillingCount;
            partyModel.KillingBiomassa += result.KillingBiomassa;
            partyModel.WastedCount += result.WastedCount;
            partyModel.WastedBiomassa += result.WastedBiomassa;
            partyModel.DifferenceCount += result.DifferenceCount;
            partyModel.DifferenceBiomassa += result.DifferenceBiomassa;
            partyModel.Increment += result.Increment;
            partyModel.IncrementNotSoldKilling += result.IncrementNotSoldKilling;
            partyModel.CountEnd += result.CountEndHitch;
            partyModel.BiomassaEnd += result.BiomassaEndHitch;
            partyModel.AmountFood += result.AmountFood;
            partyModel.InventoryCount += result.InventoryCount;
            partyModel.InventoryBiomassa += result.InventoryBiomassa;
        }

        /// <summary>
        /// Округление значений по бассейну
        /// </summary>
        /// <param name="item"></param>
        private void RoundItemModel(EveryMonthPartyModels item)
        {
            item.BiomassaStart = Math.Round(item.BiomassaStart, 1);
            item.ReceptionBiomassa = Math.Round(item.ReceptionBiomassa, 1);
            item.SoldBiomassa = Math.Round(item.SoldBiomassa, 1);
            item.KillingBiomassa = Math.Round(item.KillingBiomassa, 1);
            item.WastedBiomassa = Math.Round(item.WastedBiomassa, 1);
            item.WastedPercent = Math.Round(item.WastedPercent, 1);
            item.DifferenceBiomassa = Math.Round(item.DifferenceBiomassa, 1);

            item.Increment = Math.Round((item.BiomassaEnd - item.BiomassaStart) + item.SoldBiomassa * (-1) + item.KillingBiomassa * (-1) + item.WastedBiomassa * (-1), 1);
            item.IncrementNotSoldKilling = Math.Round(item.IncrementNotSoldKilling, 1);
            item.BiomassaEnd = Math.Round(item.BiomassaEnd, 1);
            item.AmountFood = Math.Round(item.AmountFood, 2);
            item.InventoryCount = item.InventoryCount;
            item.InventoryBiomassa = Math.Round(item.InventoryBiomassa, 1);
            // Расчет средних навесок
            item.AvgHitchStart = item.CountStart != 0 ? Math.Round(item.BiomassaStart / item.CountStart * 1000, 1) : 0;
            item.AvgHitchEnd = item.CountEnd != 0 ? Math.Round(item.BiomassaEnd / item.CountEnd * 1000, 1) : 0;
            // Расчет процентов по партиям
            item.WastedPercent = Math.Abs(item.CountStart != 0 ? Math.Round(((double)(item.WastedCount * 100) / (double)item.CountStart), 2) : 0);
            // Расчет КК
            item.KK = Math.Round((item.Increment + item.WastedBiomassa) != 0 ? item.AmountFood / (item.Increment + item.WastedBiomassa) : 0, 2);
        }

        /// <summary>
        /// Округление результатов
        /// </summary>
        /// <param name="resultEveryMonth"></param>
        private void RoundResult(EveryMonthPartyModels resultEveryMonth)
        {
            resultEveryMonth.ReceptionBiomassa = Math.Round(resultEveryMonth.ReceptionBiomassa, 1);
            resultEveryMonth.SoldBiomassa = Math.Round(resultEveryMonth.SoldBiomassa, 1);
            resultEveryMonth.KillingBiomassa = Math.Round(resultEveryMonth.KillingBiomassa, 1);
            resultEveryMonth.WastedBiomassa = Math.Round(resultEveryMonth.WastedBiomassa, 1);
            resultEveryMonth.WastedPercent = Math.Abs(Math.Round(resultEveryMonth.WastedPercent, 1));
            resultEveryMonth.DifferenceBiomassa = Math.Round(resultEveryMonth.DifferenceBiomassa, 1);
            resultEveryMonth.Increment = Math.Round(resultEveryMonth.Increment, 1);
            resultEveryMonth.IncrementNotSoldKilling = Math.Round(resultEveryMonth.IncrementNotSoldKilling, 1);
            resultEveryMonth.BiomassaStart = Math.Round(resultEveryMonth.BiomassaStart, 1);
            resultEveryMonth.BiomassaEnd = Math.Round(resultEveryMonth.BiomassaEnd, 1);
            resultEveryMonth.AmountFood = Math.Round(resultEveryMonth.AmountFood, 1);
            resultEveryMonth.InventoryBiomassa = Math.Round(resultEveryMonth.InventoryBiomassa, 1);
            // расчет средних навесок итога
            resultEveryMonth.AvgHitchStart = resultEveryMonth.CountStart != 0 ? Math.Round(resultEveryMonth.BiomassaStart / resultEveryMonth.CountStart * 1000, 1) : 0;
            resultEveryMonth.AvgHitchEnd = resultEveryMonth.CountEnd != 0 ? Math.Round(resultEveryMonth.BiomassaEnd / resultEveryMonth.CountEnd * 1000, 1) : 0;
            // Расчет процентов для итогового значения
            resultEveryMonth.WastedPercent = resultEveryMonth.CountStart != 0 ? Math.Abs(Math.Round(((double)(resultEveryMonth.WastedCount * 100) / (double)resultEveryMonth.CountStart), 2)) : 0;
            // Расчет КК
            resultEveryMonth.KK = resultEveryMonth.Increment + resultEveryMonth.WastedBiomassa != 0 ? Math.Round(resultEveryMonth.AmountFood / (resultEveryMonth.Increment + resultEveryMonth.WastedBiomassa), 2) : 0;
        }

        /// <summary>
        /// Расчет результатов
        /// </summary>
        /// <param name="resultEveryMonth"></param>
        /// <param name="item"></param>
        private void CalculateResult(EveryMonthPartyModels resultEveryMonth, EveryMonthPartyModels item)
        {
            resultEveryMonth.CountStart += item.CountStart;
            resultEveryMonth.AvgHitchStart += item.AvgHitchStart;
            resultEveryMonth.BiomassaStart += item.BiomassaStart;
            resultEveryMonth.ReceptionCount += item.ReceptionCount;
            resultEveryMonth.ReceptionBiomassa += item.ReceptionBiomassa;
            resultEveryMonth.SoldCount += item.SoldCount;
            resultEveryMonth.SoldBiomassa += item.SoldBiomassa;
            resultEveryMonth.KillingCount += item.KillingCount;
            resultEveryMonth.KillingBiomassa += item.KillingBiomassa;
            resultEveryMonth.WastedCount += item.WastedCount;
            resultEveryMonth.WastedBiomassa += item.WastedBiomassa;
            resultEveryMonth.DifferenceCount += item.DifferenceCount;
            resultEveryMonth.DifferenceBiomassa += item.DifferenceBiomassa;
            resultEveryMonth.Increment += item.Increment;
            resultEveryMonth.IncrementNotSoldKilling += item.IncrementNotSoldKilling;
            resultEveryMonth.CountEnd += item.CountEnd;
            resultEveryMonth.AvgHitchEnd += item.AvgHitchEnd;
            resultEveryMonth.BiomassaEnd += item.BiomassaEnd;
            resultEveryMonth.AmountFood += item.AmountFood;
            resultEveryMonth.InventoryCount += item.InventoryCount;
            resultEveryMonth.InventoryBiomassa += item.InventoryBiomassa;
        }

    }
}
