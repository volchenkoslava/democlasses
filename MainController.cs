﻿using lab.Infrastructure.Logging;
using lab.Infrastructure.Settings;
using lab.Infrastructure.Types;
using lab.Infrastructure.Units;
using lab.Modules.Controllers.Data;
using lab.Units.MessageEvent.Message;
using lab.Units.MessageEvent.Models;
using Prism.Events;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace lab.Modules.Controllers.Controllers
{
    /// <summary>
    /// Desktop class
    /// </summary>
    public class MainController : IMainController
    {
        private ILogging logger;
        private BlockingCollection<Task> _queue = new BlockingCollection<Task>();
        private IEventAggregator eventAggregator;
        private BackgroundWorker backgroundWorker;
        private ISerializationSettings serializationSettings;

        private MainControllerValues mainControllerValues = new MainControllerValues();

        private const int CounterMax = 30;

        public MainController(  ILogging logger,
                                IEventAggregator eventAggregator,
                                ISerializationSettings serializationSettings)
        {
            this.logger = logger;
            this.eventAggregator = eventAggregator;
            this.serializationSettings = serializationSettings;
        }

        #region Свойства
        private ControllerState _controllerState = ControllerState.NotConnection;
        public ControllerState ControllerState
        {
            get { return _controllerState; }
            set
            {
                _controllerState = value;
            }
        }

        private MeteringModeEnum _meteringMode = MeteringModeEnum.MainMode;
        public MeteringModeEnum MeteringMode
        {
            get { return _meteringMode; }
            set { _meteringMode = value; }
        }
        #endregion

        #region Управление потоком контроллера
        /// <summary>
        /// Запуск нового потока обмена информацией с главным контроллером
        /// </summary>
        public void Start()
        {
            try
            {
                if (backgroundWorker == null)
                {
                    backgroundWorker = new BackgroundWorker
                    {
                        WorkerReportsProgress = true,
                        WorkerSupportsCancellation = true
                    };

                    backgroundWorker.DoWork += WorkProcess;
                    backgroundWorker.RunWorkerAsync();
                }
                backgroundWorker.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Остновка нового потока обмена информацией с главным контроллером
        /// </summary>
        public void Stop()
        {
            try
            {
                //Переход в Main
                var meteringMode = MainControllerData.Instance().GetMeteringMode();
                int count = 0;
                while (meteringMode != MeteringModeEnum.MainMode && count < CounterMax)
                {
                    count++;
                    switch (meteringMode)
                    {
                        case MeteringModeEnum.HVMACMode:
                            StopHVM();
                            break;
                        case MeteringModeEnum.HVMDCMode:
                            StopHVM();
                            break;
                        case MeteringModeEnum.BurnMode:
                            StopBurn();
                            break;
                        case MeteringModeEnum.JoinBurn:
                            StopJoinBurn();
                            break;
                        case MeteringModeEnum.HighVoltageBurn:
                            StopHighVoltageBurn();
                            break;
                    }
                    Thread.Sleep(200);
                    meteringMode = MainControllerData.Instance().GetMeteringMode();
                }
                // Остановка выполения очереди
                if (backgroundWorker != null && backgroundWorker.IsBusy)
                {
                    backgroundWorker.CancelAsync();
                }
                // Очистка очереди
                while (_queue.Count != 0)
                {
                    _queue.Take();
                }
                // Закрытие порта
                MainControllerData.Instance().Close();
                // Изменение статуса
                ControllerState = ControllerState.NotConnection;
            }
            catch (Exception ex)
            {
                logger.WriteError(this.ToString(), ex.Message);
            }
        }

        /// <summary>
        /// Работа потока контролера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkProcess(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            while (!worker.CancellationPending)
            {
                if (ControllerState == ControllerState.NotConnection || ControllerState == ControllerState.ConnectionError)
                {
                    ControllerState = ControllerState.Initialization;
                    if (MainControllerData.Instance().InitAuto())
                    {
                        ControllerState = ControllerState.Connected;
                        // Запрос состава лаборатории
                        MainControllerMenuValues mainControllerMenuValues = MainControllerData.Instance().GetCurrentVersion();
                        eventAggregator.GetEvent<MainControllerMenuEvent>().Publish(mainControllerMenuValues);
                        // Read Settings.xml and setting the simulation value
                        MainControllerData.Instance().SetSimulation(serializationSettings.GetValueSetting("Simulation") == 0 ? false : true);
                    }
                    else
                    {
                        ControllerState = ControllerState.ConnectionError;
                        // Сброс состава лаборатории
                        MainControllerMenuValues mainControllerMenuValues = new MainControllerMenuValues();
                        eventAggregator.GetEvent<MainControllerMenuEvent>().Publish(mainControllerMenuValues);
                        // Отправка пакета с ошибкой
                        mainControllerValues.Clear();
                        mainControllerValues.Error = true;
                        eventAggregator.GetEvent<MainControllerEvent>().Publish(mainControllerValues);
                        Thread.Sleep(5000);
                    }
                }

                if (ControllerState == ControllerState.Connected)
                {
                    Task taskGetCurrentValues = new Task(() => GetCurrentValues());
                    _queue.Add(taskGetCurrentValues);

                    while (_queue.Count > 0)
                    {
                        var selectTask = _queue.FirstOrDefault();
                        if (selectTask != null)
                        {
                            try
                            {
                                selectTask.Start();
                                selectTask.Wait();
                                _queue.TryTake(out selectTask);
                            }
                            catch (Exception ex)
                            {
                                logger.WriteError(this.ToString(), ex.Message);
                            }
                        }
                    }
                }
                Thread.Sleep(50);
            }
            e.Cancel = true;
            return;
        }

        #endregion

        #region Задачи контроллера
        private void GetCurrentValues()
        {
            var currentValue = MainControllerData.Instance().GetCurrentValues();
            if (currentValue.Error)
            {
                ControllerState = ControllerState.ConnectionError;
            }
            eventAggregator.GetEvent<MainControllerEvent>().Publish(currentValue);
        }
        #endregion
    }
}
